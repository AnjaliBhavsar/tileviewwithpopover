import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class TileWithPopover extends React.Component {
 constructor(props)
 {
  super(props);
  this.state={
  showDetails:false
  };
 }
 handleMouseEvent=()=>{
   this.setState({showDetails:!this.state.showDetails})
 }
  render(){  
      const { showDetails } = this.state;
      const { name,img,contact,description ,classNamePill,classNameDetail} = this.props;
    return (
      <div className={classNamePill} onMouseEnter={this.handleMouseEvent} onMouseLeave={this.handleMouseEvent} >
      <img src={img} alt="Avatar" />
      <h6>{name}</h6>
      { showDetails&&(  <div className={classNameDetail}>
    <div >Description: {description}</div>
    <div>Contact: {contact}</div>
          </div>)}
      </div>

  );
}

}
TileWithPopover.propTypes = {
    name:PropTypes.string,
    img:PropTypes.string,
    contact:PropTypes.string,
    description:PropTypes.string,
    classNamePill:PropTypes.string,
    classNameDetail:PropTypes.string,
  };

TileWithPopover.defaultProps = { 
  name:'Produt/Person Name',
  img :'img_avatar_default.png',
  contact:'abc@xyz.com',
  description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  classNamePill:'container',
  classNameDetail:'detailText',
};

export default TileWithPopover;
